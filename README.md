C++ Basics, 5 days
====

Welcome to this course.
The syllabus can be found at
[cxx/cxx-basics](https://www.ribomation.com/courses/cxx/cxx-basics.html)

Below you will find
* Installation instructions
* Solutions to the programming exercises
* Support files
* Demo programs

Installation Instructions
====

To perform the programming exercises, you need:
* An C++ IDE, to write your code
* A modern C++ compiler, supporting C++ 17, to compile your code
* A GIT client, to get the solutions from this repo

Recommended Setup
----
Although, there are many ways to write and compile C++ programs both on Windows,
Linux and Mac, our experience of giving many courses on various levels; is that
using Ubuntu Linux provides the least amount of surprises and distracting
technical struggles.

For this course, we do recommend the following setup:
* Ubuntu Linux, version 18.04 (or later)
* GCC C++ compiler, version 8 (or later)
* [JetBrains CLion IDE, 30-days trial](https://www.jetbrains.com/clion/download/)

Ubuntu Linux (WSL) @ Windows 10
----

One of the biggest news with Windows 10, is that it provides the _Windows Subsystem for
Linux_ (WSL) plugin. Just open _Microsoft Store_ and search for "Ubuntu". Then choose
Ubuntu 18.04 and install it. It's as simple as that.

Our favorite C++ IDE is CLion and it works very nicely with WSL; i.e. you install
CLion on Windows and uses the compiler resources within WSL. Just follow the 
instructions at
* [How to Use WSL Development Environment in CLion](https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html)
* [Using WSL toolchains in CLion on Windows (YouTube)](https://youtu.be/xnwoCuHeHuY)

Ubuntu Linux @ VirtualBox
----

If you're running an earlier version of Windows, then install VirtualBox, create a
virtual machine for Ubuntu and download/install Ubuntu to it.

1. Install VirtualBox (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop 64-bit<br/>
    <http://www.ubuntu.com/download/desktop>
1. Choose as much RAM for the VM as you can, still within the "green" region.
1. Choose as much video memory you can
1. Create an auto-expanding virtual hard-drive of 50GB
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
1. Set a username and password when asked to and write them down so you remember them.
   Also, go for auto-logon.
1. Install the VBox guest additions<br/>
    <https://www.virtualbox.org/manual/ch04.html>
1. Install Clion from the Ubuntu programs installer app

GCC C++ Compiler and Tools
----
Within a Ubuntu terminal window type the following command to install the
compiler and other tools.

    sudo apt install g++-8 cmake make gdb valgrind git tree

_N.B._ when you run a `sudo` command it prompts you for the password, you use
to logon to Ubuntu.

Mac User
----

If you have a Mac laptop, install a Modern C++ compiler supporting C++ 17
and install JetBrains CLion.

Linux User
----

If you're running a different Linux desktop OS, such as Fedora or similar; use its
package manager to install a compiler supporting C++ 17 and then install  CLion.


Usage of this GIT Repo
====

Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone
this repo.

    mkdir -p ~/cxx-basics-course/my-solutions
    cd ~/cxx-basics-course
    git clone https://gitlab.com/ribomation-courses/cxx/cxx-basics.git gitlab
    cd gitlab && ls -lhFA


During the course, solutions will be push:ed to this repo and you can get these by
a git pull operation

    cd ~/cxx-basics-course/gitlab
    git pull

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

