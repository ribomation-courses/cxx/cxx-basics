#include <iostream>

using namespace std;

int main() {
    int value = 42;
    cout << "value = " << value << endl;

    int* ptr = &value;
    cout << "ptr  = " << ptr << endl;
    cout << "*ptr = " << *ptr << endl;

    *ptr = *ptr * *ptr;
    cout << "*ptr  = " << *ptr << endl;
    cout << "value = " << value << endl;

    return 0;
}
