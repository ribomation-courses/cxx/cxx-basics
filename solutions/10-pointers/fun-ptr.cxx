#include <iostream>
#include <cstdio>
#include <string>
using namespace std;
using namespace std::literals;

using XXL = unsigned long long;
using XS = unsigned short;
using FunPtr = XXL (*)(XS);

XXL sum(XS n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return n + sum(n - 1);
}

XXL prod(XS n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return n * prod(n - 1);
}

XXL fib(XS n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fib(n - 2) + fib(n - 1);
}

int main(int argc, char** argv) {
    auto const N      = (argc <= 1) ? 10 : stoi(argv[1]);
    FunPtr     funs[] = {&sum, &fib, &prod};
    for (auto  f : funs) {
        printf("[C  ] f(%d) = %llu\n", N, (*f)(N));
        cout << "[C++] f(" << N << ") = " << f(N) << endl;
    }
    return 0;
}
