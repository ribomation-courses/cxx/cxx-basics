#include <iostream>

using namespace std;

struct Account {
    int    balance = 100;
    double rate    = 1.25;
};

int main() {
    cout << "sizeof(Account) = " << sizeof(Account)
         << ", sizeof(balance) = " << sizeof(Account::balance)
         << ", sizeof(rate) = " << sizeof(Account::rate)
         << endl;

    Account acc = {1500, 2.35};
    cout << "acc: SEK " << acc.balance << ", " << acc.rate << "%\n";

    cout << "-------------\n";
    Account* ptr = &acc;
    cout << "*ptr = SEK " << (*ptr).balance << ", " << (*ptr).rate << "%\n";

    cout << "-------------\n";
    ptr->balance *= 10;
    ptr->rate *= 2;
    cout << "*ptr = SEK " << ptr->balance << ", " << ptr->rate << "%\n";

    cout << "-------------\n";
    cout << "ptr          = " << ptr << endl;
    cout << "&ptr.balance = " << &((*ptr).balance) << endl;
    cout << "&ptr.rate    = " << &(ptr->rate) << endl;
    cout << "delta addr   = "
         << (reinterpret_cast<unsigned long>(&(ptr->rate)) - reinterpret_cast<unsigned long>(&(ptr->balance))) << endl;

    return 0;
}
