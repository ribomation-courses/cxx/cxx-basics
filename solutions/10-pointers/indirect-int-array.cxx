#include <iostream>

using namespace std;

int main() {
    int        arr[] = {42, 0, 2, 3, 5, 8, 13, 21, 34, 55};
    const auto N     = sizeof(arr) / sizeof(arr[0]);
    cout << "N=" << N << endl;

    for (auto k = 0U; k < N; ++k) {
        cout << "arr[" << k << "] = " << arr[k] << endl;
    }

    cout << "-------------\n";
    int* ptr = &(arr[0]);  // ptr=arr
    cout << "*ptr = " << *ptr << endl;
    cout << "*ptr = " << *(&(arr[0])) << endl;
    cout << "*ptr = " << *arr << endl;

    cout << "-------------\n";
    for (int* q = arr; q < &arr[N]; ++q) {
        cout << "*q = " << *q << endl;
    }

    cout << "-------------\n";
    for (int* q = arr; q < (arr + N); ++q) {
        *q *= 10;
    }
    for (int* q = arr; q < &arr[N]; ++q) {
        cout << "*q = " << *q << endl;
    }

    cout << "-------------\n";
    for (auto k = 0U; k < N; ++k) {
        cout << "arr[" << k << "] = " << arr[k] << endl;
    }

    return 0;
}
