#include <iostream>
#include <string>
using namespace std;

int finalAmount(int a, float p) {
    return static_cast<int>(a * (1 + p / 100));
}

int initialAmount(int a, float p) {
    return static_cast<int>(a * (1 / (1 + p / 100)));
}

int main(int argc, char** argv) {
    auto amount = 1000;
    auto rate   = 1.5F;

    if (argc > 1) amount = stoi(argv[1]);
    if (argc > 2) rate   = stof(argv[2]);

    cout << "Amount: SEK " << amount << endl;
    cout << "Rate  : " << rate << "%" << endl;

    auto a1 = finalAmount(amount, rate);
    cout << "Final Amount: SEK " << a1 << endl;

    auto a0 = initialAmount(a1, rate);
    cout << "Init  Amount: SEK " << a0 << endl;

    return 0;
}
