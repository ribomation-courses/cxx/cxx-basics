#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <functional>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    void each(istream& in, const function<void(string)>& use) {
        for (string line; getline(in, line);) use(line);
    }

    void each(const string& filename, const function<void(string)>& use) {
        ifstream file{filename};
        if (!file) throw invalid_argument{"Cannot open "s + filename};
        each(file, use);
    }

}
