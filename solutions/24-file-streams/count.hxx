#pragma once
#include <iosfwd>
#include <iomanip>
#include <sstream>
#include <string>
#include <utility>

namespace ribomation {
    using namespace std;

    struct Count {
        const string file;
        unsigned     lines = 0;
        unsigned     words = 0;
        unsigned     chars = 0;

        Count(string f) : file(move(f)) {}

        void aggregate(const string& line) {
            ++lines;
            words += countWords(line);
            chars += line.size();
        }

        unsigned countWords(const string& line) {
            istringstream buf{line};
            unsigned      count = 0;
            for (string   w; buf >> w; ++count);
            return count;
        }

        void aggregate(const Count& that) {
            lines += that.lines;
            words += that.words;
            chars += that.chars;
        }

        Count& operator +=(const string& line) {
            aggregate(line);
            return *this;
        }

        Count& operator +=(const Count& that) {
            aggregate(that);
            return *this;
        }

        string toString() const {
            const char    TAB = '\t';
            ostringstream buf;
            buf << setw(5) << right << lines
                << TAB << setw(5) << right << words
                << TAB << setw(5) << right << chars
                << TAB << file;
            return buf.str();
        }

        friend inline ostream& operator <<(ostream& os, const Count& cnt) {
            return os << cnt.toString();
        }
    };

}
