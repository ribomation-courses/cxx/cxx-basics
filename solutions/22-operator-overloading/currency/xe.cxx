#include <iostream>
#include <utility>
#include <set>

#include "Currency.hxx"
#include "Money.hxx"

using namespace std;

Currency sek{"SEK", 1};
Currency eur{"EUR", 0.107362862};
Currency usd{"USD", 0.120153};

void print_currencies() {
    cout << "-- Currency Objects --\n";
    cout << sek << endl;
    cout << eur << endl;
    cout << usd << endl;
}

void print_set_of_currencies() {
    cout << "-- Currency Names --\n";
    set<Currency> currencies = {usd, eur, sek};
    for (auto &c : currencies) { cout << c.name() << endl; }
}

void type_conversions() {
    cout << "-- Type Conversions of Currencies --\n";

    string name = usd; //usd.operator string()
    double amount = usd; //usd.operator double() --> rate() --> xe
    cout << "name  : " << name << endl;
    cout << "rate: " << amount << endl;
}

void print_money() {
    cout << "-- Money Objects --\n";
    Money s{10, sek};
    Money e{25, eur};
    Money u{10, usd};

    cout << "s: " << s << endl;
    cout << "e: " << e << endl;
    cout << "u: " << u << endl;
}

void convert() {
    cout << "-- Currency Exchange --\n";
    Money s{10, sek};

    cout << s << " --> " << s / sek << endl;
    cout << s << " --> " << s / eur << endl;
    cout << s << " --> " << s / usd << endl;

    usd = 0.420153;
    cout << "s/USD --> " << s / usd << endl;
}

void add_sub() {
    Money s{10, sek};
    Money s2{20, sek};
    Money u{10, usd};

    cout << "-- Addition --\n";
    cout << s << " + " << s2 << " --> " << (s + s2) << endl;
    cout << s << " + " << u << " --> " << (s + u) << endl;
    cout << u << " + " << s << " --> " << (u + s) << endl;

    cout << "-- Subtraction --\n";
    cout << s2 << " - " << s << " --> " << (s2 - s) << endl;
    cout << u << " - " << s << " --> " << (u - s) << endl;
}

void mult() {
    Money s{10, sek};
    cout << "-- Multiplication with factor --\n";
    cout << 42 << " * " << s << " --> " << (42 * s) << endl;
    cout << s << " * " << 42 << " --> " << (s * 42) << endl;
}

int main() {
    print_currencies();
    print_set_of_currencies();
    type_conversions();
    print_money();
    convert();
    add_sub();
    mult();
    return 0;
}
