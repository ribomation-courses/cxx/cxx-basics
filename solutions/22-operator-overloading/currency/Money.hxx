#pragma once

#include <iostream>
#include <iomanip>
#include "Currency.hxx"


class Money {
    double value;
    const Currency& cur;

public:
    Money(double val, const Currency& cur) : value{val}, cur{cur} { }

    Money() = delete;
    ~Money() = default;
    Money(const Money&) = default;
    Money& operator =(const Money&) = delete;

    double amount() const {
        return value;
    }

    const Currency& currency() const {
        return cur;
    }

    Money convert(const Currency& to) const {
        return {value * to.rate() / cur.rate(), to};
    }

    friend ostream& operator <<(ostream& os, const Money& m) {
        return os << m.currency().name() << " " << fixed << setprecision(2) << m.amount();
    }
};

inline Money operator /(const Money& left, const Currency& right) {
    return left.convert(right);
}

inline Money operator +(const Money& left, const Money& right) {
    if (left.currency() == right.currency()) {
        return {left.amount() + right.amount(), left.currency()};
    }

    return left / right.currency() + right;
}

inline Money operator-(const Money& m) {
    return {-m.amount(), m.currency()};
}

inline Money operator-(const Money& left, const Money& right) {
    if (left.currency() == right.currency()) {
        return left + -right;
    }
    return left / right.currency() - right;
}

inline Money operator*(double factor, const Money& m) {
    return {factor * m.amount(), m.currency()};
}

inline Money operator*(const Money& m, double factor) {
    return factor * m;
}




