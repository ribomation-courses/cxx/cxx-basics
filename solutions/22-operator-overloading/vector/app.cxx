#include <iostream>
#include <sstream>
#include "vector-3d.hxx"
using std::cout;
using std::endl;
using std::istringstream;
using ribomation::Vector3d;

void test_non_float() {
    //Vector3d<int> v1{2, 5, 10};
    //Vector3d<std::string>  v;
}

int main() {
    Vector3d<> v1{2, 5, 10};
    cout << "v1: " << v1 << endl;

    istringstream{"data: <10 20 30>"} >> v1;
    cout << "v1: " << v1 << endl;

    cout << "v1 * 42: " << v1 * 42. << endl;
    cout << "10 * v1: " << 10. * v1 << endl;

    Vector3d<> v2{2, 2, 2};
    cout << "v1 + v2: " << v1 + v2 << endl;
    cout << "v1 - v2: " << v1 - v2 << endl;
    cout << "v1 * v2: " << v1 * v2 << endl;

    test_non_float();
    return 0;
}
