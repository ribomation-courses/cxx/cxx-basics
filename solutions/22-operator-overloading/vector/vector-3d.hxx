#pragma once
#include <iostream>
#include <type_traits>
#include <cctype>

namespace ribomation {
    using std::is_floating_point_v;
    using std::ostream;
    using std::istream;

    template<typename T = double>
    class Vector3d {
        T x = {};
        T y = {};
        T z = {};

    public:
#define CHECK_TYPE static_assert(is_floating_point_v<T>, "Type parameter must be numeric")

        Vector3d() { CHECK_TYPE; }
        Vector3d(T x, T y, T z) : x{x}, y{y}, z{z} { CHECK_TYPE; }
        Vector3d(const Vector3d<T>&) = default;
        ~Vector3d() = default;
        Vector3d<T>& operator =(const Vector3d<T>&) = default;

        T getX() const { return x; }
        T getY() const { return y; }
        T getZ() const { return z; }

        friend ostream& operator <<(ostream& os, const Vector3d<T>& v) {
            return os << "[" << v.x << ", " << v.y << ", " << v.z << "]";
        }

        friend istream& operator >>(istream& is, Vector3d<T>& vec) {
            char ch;
            while (is.get(ch) && !isdigit(ch));
            is.putback(ch);

            Vector3d<T> v{};
            is >> v.x >> v.y >> v.z;
            vec = v;

            return is;
        }
    };

    template<typename T>
    Vector3d<T> operator *(const Vector3d<T>& v, T factor) {
        return {v.getX() * factor,
                v.getY() * factor,
                v.getZ() * factor};
    }

    template<typename T>
    Vector3d<T> operator *(T factor, const Vector3d<T>& v) {
        return v * factor;
    }

    template<typename T>
    Vector3d<T> operator +(const Vector3d<T>& lhs, const Vector3d<T>& rhs) {
        return {lhs.getX() + rhs.getX(),
                lhs.getY() + rhs.getY(),
                lhs.getZ() + rhs.getZ()};
    }

    template<typename T>
    Vector3d<T> operator -(const Vector3d<T>& lhs, const Vector3d<T>& rhs) {
        return {lhs.getX() - rhs.getX(),
                lhs.getY() - rhs.getY(),
                lhs.getZ() - rhs.getZ()};
    }

    template<typename T>
    T operator *(const Vector3d<T>& lhs, const Vector3d<T>& rhs) {
        return lhs.getX() * rhs.getX()
             + lhs.getY() * rhs.getY()
             + lhs.getZ() * rhs.getZ();
    }

}
