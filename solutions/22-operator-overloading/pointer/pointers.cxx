#include <iostream>
#include <string>
#include "Ptr.hxx"


struct Person {
    string name;

    Person() {
        cout << "Person() @ " << this << endl;
    }

    Person(const string& s) : name(s) {
        cout << "Person{" << name << "} @ " << this << endl;
    }

    ~Person() {
        cout << "~Person(" << name << ") @ " << this << endl;
    }

    string toString() const {
        return "Person{" + name + "} @ " + to_string(reinterpret_cast<unsigned long>(this));
    }
};


int main() {
    Ptr<Person> p{new Person{"Anna Conda"}};
    cout << "p: " << p->toString() << endl;  // ( p.operator ->() )->toString()

    Ptr<Person> q{new Person{"Per Silja"}};
    cout << "q: " << (*q).toString() << endl;  // ( p.operator *() ).toString()

    {
        cout << "-----------\n";
        Ptr<Person> r;
        r = p;
        cout << "r: " << r->toString() << endl;
        try {
            cout << "p: " << p->toString() << endl;
        } catch (invalid_argument& x) {
            cout << "*** " << x.what() << endl;
        }
    }

    cout << "-----------\n";
    cout << "q: " << q->toString() << endl;
    q = nullptr;
    try {
        cout << "q: " << q->toString() << endl;
    } catch (invalid_argument& x) {
        cout << "*** " << x.what() << endl;
    }
}
