#include <iostream>
#include <string>
#include <functional>
#include <algorithm>

using namespace std;

void print(const string& name, int* numbers, unsigned N) {
    cout << name << ": " ;
    for_each(numbers, numbers + N, [](auto x) { cout << x << " "; });
    cout << "\n";
}

double reduce(int* arr, const unsigned n, function<double(double, int)> aggregator) {
    double    result = arr[0];
    for (auto k      = 1U; k < n; ++k) { result = aggregator(result, arr[k]); }
    return result;
}


int main() {
    int            numbers[] = {10, 2, 35, 42, 5};
    const unsigned size  = sizeof(numbers) / sizeof(numbers[0]);
    print("nums", numbers, size);

    auto sum = reduce(numbers, size, [](auto acc, auto n) { return acc + n; });
    cout << "sum : " << sum << endl;

    auto prod = reduce(numbers, size, [](auto acc, auto n) { return acc * n; });
    cout << "prod: " << prod << endl;

    auto max = reduce(numbers, size, [](auto acc, auto n) { return n > acc ? n : acc; });
    cout << "max : " << max << endl;

    auto min = reduce(numbers, size, [](auto acc, auto n) { return n < acc ? n : acc; });
    cout << "min : " << min << endl;

    return 0;
}



