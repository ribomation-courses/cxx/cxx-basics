#include <iostream>
#include <string>
#include <algorithm>

using namespace std;
using namespace std::literals;

int main() {
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    const auto N  = sizeof(numbers) / sizeof(numbers[0]);

    for_each(numbers, numbers + N, [](auto x) { cout << x << " "; });
    cout << endl;

    auto factor = 42;
    transform(numbers, numbers + N, numbers, [factor](auto x) { return x * factor; });

    for_each(numbers, numbers + N, [](auto x) { cout << x << " "; });
    cout << endl;

    return 0;
}
