#include <iostream>
#include <iterator>
#include <set>
#include <vector>
#include <string>
#include <algorithm>
#include <cctype>
#include <numeric>

using namespace std;
using namespace std::literals;

//usage: cat file | ./words
int main() {
    auto payload = vector<string>{istream_iterator<string>{cin}, istream_iterator<string>{}};

    auto words = set<string>{};
    transform(begin(payload), end(payload), inserter(words, words.begin()), [](auto& w) {
        auto result = ""s;
        for_each(begin(w), end(w), [&](auto ch) { if (isalpha(ch)) result += ch; });
        return result;
    });

    copy(begin(words), end(words), ostream_iterator<string>{cout, "\n"});
    cout << endl;
accumulate()
    return 0;
}
