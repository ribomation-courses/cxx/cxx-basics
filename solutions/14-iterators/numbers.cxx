#include <iostream>
#include <iterator>
using namespace std;

//usage: seq 10 | ./numbers
int main() {
    // Person p
    istream_iterator<unsigned> in{cin};  //int n; cin >> n;
    istream_iterator<unsigned> eof;
    ostream_iterator<unsigned> out{cout, " "};

    for (; in != eof; ++in) {
        auto n = *in;
        *out = n * n;
    }
    cout << endl;

    return 0;
}
