#include <iostream>
#include <cstdlib>
#include <bitset>

using namespace std;

unsigned genSecret() {
    return static_cast<unsigned>(1 + (rand() % 10));
}

int main() {
    srand(static_cast<unsigned>(time(nullptr)));
//    auto x = static_cast<unsigned>(-1024);
//    cout << "x=" << x << endl;
//    cout << "x=" << bitset<32>(x) << endl;

    const auto maxGuesses = 5;
    auto       secret     = genSecret();
    auto       numGuesses = 0;
    do {
        cout << "Guess: ";
        unsigned guess;
        cin >> guess;
        ++numGuesses;

        if (guess == secret) {
            cout << "Correct!\n";
            cout << "# guesses = " << numGuesses << endl;
            return 0;
        }

        if (guess < secret) cout << "Too low" << endl;
        if (guess > secret) cout << "Too high" << endl;
    } while (numGuesses < maxGuesses);

    cout << "Secret    = " << secret << endl;
    cout << "# guesses = " << numGuesses << endl;

    return 1;
}
