cmake_minimum_required(VERSION 3.10)
project(21_templates)

set(CMAKE_CXX_STANDARD 17)
#add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(equals equals.cxx)
add_executable(simple-stack simple-stack.cxx)
