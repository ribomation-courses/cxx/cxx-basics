#include <iostream>
#include <string>
using namespace std;

template<typename T = int, unsigned N = 20>
class Stack {
    T        stk[N];
    unsigned top = 0;
public:
    void        push(T x) { stk[top++] = x; }
    T           pop()     { return stk[--top]; }
    bool        empty()    const { return top == 0; }
    bool        full()     const { return top >= N; }
    unsigned    capacity() const { return N; }
};

struct Person {
    string name;
    Person(string name) : name{name} {}
    Person() = default;
    ~Person() = default;
    Person(const Person&) = default;
    friend ostream& operator<<(ostream& os, const Person& p) {
        return os << p.name;
    }
};


template<typename E, unsigned N>
void drain(string label, Stack<E,N>& stk) {
    cout << label << ": ";
    while (!stk.empty()) cout << stk.pop() << " ";
    cout << endl;
}


void stack_of_ints() {
    Stack<>   s;
    for (auto k = 1; !s.full(); ++k) s.push(k);
    drain("ints   ", s);
}

void stack_of_string() {
    using namespace std::literals;
    Stack<string, 15> s;
    for (auto k = s.capacity(); !s.full(); --k) s.push("txt-"s + to_string(k));
    drain("strings", s);
}

void stack_of_persons() {
    using namespace std::literals;
    Stack<Person, 12> s;
    for (auto k = s.capacity(); !s.full(); --k) s.push(Person{"Nisse-"s + to_string(k)});
    drain("persons", s);
}

int main(int, char**) {
    stack_of_ints();
    stack_of_string();
    stack_of_persons();
    return 0;
}

