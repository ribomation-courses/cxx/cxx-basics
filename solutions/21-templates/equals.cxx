#include <iostream>
#include <iomanip>
#include <string>

template<typename T>
bool equals(T a, T b, T c) {
    std::cout << __PRETTY_FUNCTION__ << " --> ";
    return (a == b) && (b == c);
}

int main() {
    using namespace std;
    using namespace std::literals;

    cout << boolalpha
         << "equals<int>: "
         << equals(21, 3 * 7, 2 * 10 + 1)
         << endl;

    cout << boolalpha
         << "equals<int>: "
         << equals<unsigned char>(21, 3 * 7, 2 * 10 + 1)
         << endl;

    cout << boolalpha
         << "equals<float>: "
         << equals(21.0, 3 * 7.0, 2 * 10 + 1.0)
         << endl;

    cout << boolalpha
         << "equals<float>: "
         << equals<float>(21.0, 3 * 7.0, 2 * 10 + 1.0)
         << endl;

    cout << boolalpha
         << "equals<string>: "
         << equals("hello"s, "he"s + "llo"s, "hello world"s.substr(0, 5))
         << endl;

    return 0;
}
