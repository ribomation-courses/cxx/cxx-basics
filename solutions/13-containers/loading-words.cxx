#include <iostream>
#include <string>
#include <vector>
#include <cctype>

using namespace std;
using namespace std::literals;


int main() {
    vector<string> words;

    cout << "--- Raw words --------------\n";
    for (string word; cin >> word;) {
        words.push_back(word);
    }
    for (auto& w : words) cout << w << " ";
    cout << endl;


    cout << "--- Stripped words --------------\n";
    for (auto& word : words) {
        string    stripped;
        for (auto ch : word) if (::isalpha(ch)) stripped += ch;
        word = stripped;
    }
    for (auto& w : words) cout << w << " ";
    cout << endl;


    cout << "--- Total length --------------\n";
    auto totLength = 0U;
    for (auto& w : words) totLength += w.size();
    cout << "Total length: " << totLength << endl;


    cout << "--- New word at front --------------\n";
    words.insert(words.begin(), "Tjolla-Hopp"s);
    for (auto& w : words) cout << w << " ";
    cout << endl;


    cout << "--- Emptying and printing --------------\n";
    while (!words.empty()) {
        auto word = words.back();
        words.pop_back();
        cout << word << " ";
    }
    cout << endl;

    return 0;
}
