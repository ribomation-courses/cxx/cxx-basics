#include <iostream>
#include <string>
#include <map>

using namespace std;

string strip(string s) {
    string    result;
    for (auto ch : s) if (isalpha(ch)) result += ch;
    return result;
}

int main() {
    map<string, unsigned> freqs;

    for (string word; cin >> word;) {
        word = strip(word);
        if (word.size() >= 4) freqs[word]++; // (freq.operator[](word))++
    }

    for (const auto& [word, freq] : freqs) {
        cout << word << ": " << freq << endl;
    }

    return 0;
}
