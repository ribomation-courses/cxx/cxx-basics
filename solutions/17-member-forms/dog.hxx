#pragma once
#include <iostream>
#include <string>

namespace persons {
    using namespace std;
    using namespace std::literals;

    class Dog {
        const string name;
    public:
        Dog(string name) : name{move(name)} {}

        string getName() const { return name; }

        void bark() const {
            cout << name << ": Voff Voff!\n";
        }
    };
}
