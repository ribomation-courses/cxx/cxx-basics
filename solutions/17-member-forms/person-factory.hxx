#pragma once
#include <string>
#include <vector>
#include <random>
#include "person.hxx"

namespace persons {
    using namespace std;
    using namespace std::literals;

    class PersonFactory {
        random_device r;

    public:
        Person create() {
            return {nextName(), nextAge()};
        }

        string nextName() {
            static vector <string> names = {"Anna"s, "Berit"s, "Carin"s, "Doris"s, "Eva"s, "Frida"s};
            static unsigned const  N     = names.size() - 1;

            static uniform_int_distribution<unsigned> nextNameIdx{0, N};
            return names[nextNameIdx(r)];
        }

        unsigned nextAge() {
            static uniform_int_distribution<unsigned> nextAge{20, 80};
            return nextAge(r);
        }
    };
}
