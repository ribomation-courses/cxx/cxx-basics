#pragma  once
#include <string>
#include <sstream>
#include "dog.hxx"

namespace persons {
    using namespace std;
    using namespace std::literals;

    class Person {
        string      name;
        unsigned    age;
        Dog*        dog = nullptr;
        static int  instanceCount;

    public:
        Person(string name, unsigned age=23) : name{move(name)}, age{age} {
            ++instanceCount;
        }
        Person(const Person& that) : name{that.name}, age{that.age}, dog{that.dog} {
            ++instanceCount;
        }
        ~Person() {
            --instanceCount;
        }

        Dog* getDog() const { return dog; }
        void setDog(Dog* dog_) { Person::dog = dog_; }

        string getName() const { return name; }
        void setName(const string& name_) { Person::name = name_; }

        unsigned getAge() const { return age; }
        void setAge(unsigned age_) { Person::age = age_; }

        static int getCount() { return instanceCount; }

        string toString() {
            ostringstream buf;
            buf << "Person{"
                << getName() << ", " << getAge()
                << (getDog() == nullptr ? "" : ", has a dog "s + getDog()->getName())
                << "}";
            return buf.str();
        }
    };

}
