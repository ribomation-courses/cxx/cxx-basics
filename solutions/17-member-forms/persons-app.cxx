#include <iostream>
#include <string>
#include <vector>
#include "person.hxx"
#include "person-factory.hxx"

using namespace std;
using namespace std::literals;
using namespace persons;

void single() {
    auto fido = Dog{"Fido"};
    fido.bark();

    auto factory = PersonFactory{};
    auto p       = factory.create();
    cout << "p: " << p.toString() << endl;
    cout << "Person::cnt = " << Person::getCount() << endl;

    p.setDog(&fido);
    cout << "p: " << p.toString() << endl;
    p.getDog()->bark();

    p.setDog(nullptr);
    cout << "p: " << p.toString() << endl;
}

void many(unsigned N) {
    cout << "[many] (1) Person::cnt = " << Person::getCount() << endl;
    auto persons = vector<Person>{};
    persons.reserve(N);

    auto f = PersonFactory{};
    while (N-- > 0) {
        //persons.emplace_back(f.nextName(), f.nextAge());
        persons.push_back(f.create());
    }
    cout << "[many] (2) Person::cnt = " << Person::getCount() << endl;

    while (!persons.empty()) {
        auto p = persons.back();
        cout << "[many] p: " << p.toString() << " @ " << &p << endl;
        persons.pop_back();
    }
    cout << "[many] (3) Person::cnt = " << Person::getCount() << endl;
}

void implicit()  {
    auto persons = vector<Person>{
        "Nisse"s, "Olle"s, "Berra"s
    };
    for (auto& p : persons) cout << p.toString() << " @ " << &p << endl;
    cout << "[implicit] Person::cnt = " << Person::getCount() << endl;
}

int main() {
    cout << "[main] Person::cnt = " << Person::getCount() << endl;
    single();

    cout << "[main] Person::cnt = " << Person::getCount() << endl;
    cout << "[main] -------------\n";

    many(10);
    cout << "[main] -------------\n";
    implicit();

    cout << "[main] Person::cnt = " << Person::getCount() << endl;
    return 0;
}
