#include <iostream>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace persons;

Person data{"data", 10};
int main() {
    Person stack{"stack", 20};
    Person* heap = new Person{"heap", 30};
    delete heap;
    return 0;
}
