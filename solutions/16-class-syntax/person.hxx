#pragma  once

#include <string>

namespace persons {
    using namespace std;
    using namespace std::literals;

    class Person {
        string   name;
        unsigned age;
    public:
        Person(string name_, unsigned age_) : name{name_}, age{age_} {
            cout << "Person{" << name << ", " << age << "} @ " << this << endl;
        }
        Person(const Person& that) : name{that.name}, age{that.age} {
            cout << "Person{const Person& " << name << ", " << age << "} @ " << this << endl;
        }

        ~Person() {
            cout << "~Person{} @ " << this << endl;
        }

        string getName() const { return name; }

        unsigned getAge() const { return age; }

        void setName(const string& name_) { Person::name = name_; }

        void setAge(unsigned age_) { this->age = age_; }

        string toString() {
            return "Person{"s + getName() + ", "s + to_string(getAge()) + "}"s;
        }
    };

}
