#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "person.hxx"
#include "person-factory.hxx"

using namespace std;
using namespace std::literals;
using namespace persons;

int main(int argc, char** argv) {
    const auto N = (argc == 1) ? 32U : stoi(argv[1]);

    {
        auto factory = PersonFactory{};
        auto pers    = vector<Person>{};
        pers.reserve(N);
        cout << "-- create --\n";
        for (auto k = 0U; k < N; ++k) {
            //pers.push_back(factory.create());
            pers.emplace_back(factory.nextName(), factory.nextAge());
        }
        cout << "-- print --\n";
        for (auto& p : pers) cout << p.toString() << endl;
        cout << "-- dispose --\n";
    }

    cout << "[main] exit\n";
    return 0;
}
