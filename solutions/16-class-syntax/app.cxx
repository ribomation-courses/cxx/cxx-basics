#include <iostream>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace persons;

void func(Person& p) {
    cout << "[func] p: " << p.toString() << " @ " << &p << endl;
    p.setAge(p.getAge() + 1);
    cout << "[func] p: " << p.toString() << " @ " << &p << endl;
}

int main() {
    cout << "[main] enter\n";
    {
        auto p1 = Person{"Nisse", 42};
        cout << "p1: " << p1.toString() << endl;

        p1.setName("Bosse");
        p1.setAge(p1.getAge() + 1);
        cout << "p1: " << p1.toString() << endl;

        auto p2 = Person{"Anna", 37};
        cout << "p2: " << p2.toString() << endl;

        auto p3 = Person{"Eva", 44};

        p2.setAge(p2.getAge() + 1);
        cout << "p2: " << p2.toString() << endl;

        func(p2);
        cout << "p2: " << p2.toString() << endl;
    }
    cout << "[main] exit\n";
    return 0;
}
