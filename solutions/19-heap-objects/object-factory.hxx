#pragma once

#include <string>
#include <vector>
#include <random>
#include "person.hxx"

namespace persons {
    using namespace std;
    using namespace std::literals;

    class ObjectFactory {
        random_device r;

    public:
        Person* createPerson() {
            return new Person{nextPersonName(), nextAge()};
        }

        Dog* createDog() {
            return new Dog{nextDogName()};
        }

        string nextPersonName() {
            static auto     personNames = vector<string>{"Anna"s, "Berit"s, "Carin"s, "Doris"s, "Eva"s, "Frida"s};
            static unsigned N           = personNames.size() - 1;
            static auto     nextNameIdx = uniform_int_distribution<unsigned>{0, N};
            return personNames[nextNameIdx(r)];
        }

        string nextDogName() {
            static auto     dogNames    = vector<string>{"Fido"s, "Caro"s, "Lufsen"s, "Woff"s};
            static unsigned N           = dogNames.size() - 1;
            static auto     nextNameIdx = uniform_int_distribution<unsigned>{0, N};
            return dogNames[nextNameIdx(r)];
        }

        unsigned nextAge() {
            static auto nextAge = uniform_int_distribution<unsigned>{20, 80};
            return nextAge(r);
        }

        bool yesOrNo() {
            return uniform_int_distribution<unsigned char>{0, 1}(r) == 1;
        }

        ObjectFactory() = default;
        ~ObjectFactory() = default;
        ObjectFactory(const ObjectFactory&) = delete;
        ObjectFactory& operator =(const ObjectFactory&) = delete;
    };

}

