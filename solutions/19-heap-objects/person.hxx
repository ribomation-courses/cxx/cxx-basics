#pragma  once

#include <string>
#include <sstream>
#include "dog.hxx"

namespace persons {
    using namespace std;
    using namespace std::literals;

    class Person {
        string   name;
        unsigned age;
        Dog* dog = nullptr;
        static int instanceCount;

    public:
        Person(string name, unsigned age = 42) : name{name}, age{age} {
            ++instanceCount;
            cout << "Person(" << name << "," << age << ") @ " << this << " [" << instanceCount << "]" << endl;
        }

        Person(const Person& that) : name{that.name}, age{that.age}, dog{that.dog} {
            ++instanceCount;
            cout << "Person(const Person&: " << &that << ") @ " << this << " [" << instanceCount << "]" << endl;
        }

        ~Person() {
            if (dog) {
                delete dog;
            }
            --instanceCount;
            cout << "~Person() @ " << this << " [" << instanceCount << "]" << endl;
        }

        operator string() const {
            return name;
        }

        Dog* getDog() const { return dog; }
        void setDog(Dog* dog) { Person::dog = dog; }
        bool hasDog() const { return Person::dog != nullptr; }

        string getName() const { return name; }
        void setName(const string& name) { Person::name = name; }

        unsigned getAge() const { return age; }
        void setAge(unsigned age) { Person::age = age; }

        static int getCount() { return instanceCount; }

        string toString() {
            ostringstream buf;
            buf << "Person{"
                << getName() << ", " << getAge()
                << (getDog() == nullptr ? "" : ", dog: "s + getDog()->getName())
                << "}"
                << " [" << getCount() << "]";
            return buf.str();
        }
    };

}
