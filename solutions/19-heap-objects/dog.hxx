#pragma once

#include <iostream>
#include <string>

namespace persons {
    using namespace std;
    using namespace std::literals;

    class Dog {
        const string name;

    public:
        Dog(const string& name) : name(name) {
            cout << "Dog{" << name << "} @ " << this << endl;
        }

        ~Dog() {
            cout << "~Dog() @ " << this << endl;
        }

        string getName() const { return name; }

        void bark() const { cout << getName() << ": Voff Voff!\n"; }

        Dog() = delete;
        Dog(const Dog&) = delete;
        Dog& operator =(const Dog&) = delete;
    };
}
