#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include "dog.hxx"
#include "person.hxx"
#include "object-factory.hxx"
using namespace std;
using namespace std::literals;
using namespace persons;

int main(int argc, char** argv) {
    auto const N = (argc == 1) ? 200U : stoi(argv[1]);

    auto factory = ObjectFactory{};
    cout << "# persons = " << Person::getCount() << endl;
    {
        auto persons = vector<Person*>{};
        persons.reserve(N);
        for (auto k = 0U; k < N; ++k) {
            auto p = factory.createPerson();
            if (factory.yesOrNo()) {
                p->setDog(factory.createDog());
            }
            persons.push_back(p);
        }
        cout << "# persons = " << Person::getCount() << endl;

        cout << "----------------\n";
        for_each(persons.begin(), persons.end(), [](auto p) {
            cout << p->toString() << endl;
            if (p->hasDog()) p->getDog()->bark();
        });

        cout << "----------------\n";
        for_each(persons.begin(), persons.end(), [](auto p) {
            delete p;
        });
    }
    //auto leak = factory.createPerson();
    cout << "# persons = " << Person::getCount() << endl;

    return 0;
}
