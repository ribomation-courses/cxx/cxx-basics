#include <iostream>

using namespace std;

int main() {
    cout << "Give amount: ";
    int amount;
    cin >> amount;

    cout << "Give rate: ";
    float rate;
    cin >> rate;

    auto result = amount * (1 + rate / 100.);
    cout << "Amount: " << amount
         << ", Rate: " << rate
         << " --> Final Amount = " << result
         << endl;

    return 0;
}
