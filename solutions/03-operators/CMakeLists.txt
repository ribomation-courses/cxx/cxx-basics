cmake_minimum_required(VERSION 3.10)
project(03_operators LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)

add_executable(amounts amounts.cxx)
target_compile_options(amounts PRIVATE
    -Wall -Wextra -Werror -Wfatal-errors
    )
