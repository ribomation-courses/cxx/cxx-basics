#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include "instance-count.hxx"

using namespace std;

class Account : public instance {
    const string accno;
    int          balance;

public:
    Account(const string& accno, int balance = 100)
            : accno{accno}, balance{balance} {
        cout << "Account{" << accno << ", " << balance << "} @ " << this << endl;
    }

    ~Account() {
        cout << "~Account() @ " << this << endl;
    }

    Account() = delete;
    Account(const Account&) = delete;
    Account& operator=(const Account&) = delete;

    Account& operator+=(int amount) {
        balance += amount;
        return *this;
    }

    operator const string() const {
        return accno;
    }

    operator int() const {
        return balance;
    }

    string toString() const {
        ostringstream buf;
        buf << "Account{" << accno << ", SEK " << balance << "} [#objs=" << instance::numObjs() << "]";
        return buf.str();
    }

    friend ostream& operator<<(ostream& os, const Account& a) {
        return os << a.toString();
    }
};

