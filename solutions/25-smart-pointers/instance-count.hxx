#pragma  once

class instance {
    static int count;
    //N.B. don't forget to allocate space for the counter in a *.cxx file
    //int instance-count::count = 0;

protected:
    instance() { count++; }

public:
    virtual ~instance() { count--; }
    static int numObjs() { return count; }
};
