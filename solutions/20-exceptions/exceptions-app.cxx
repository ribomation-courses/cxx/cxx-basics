#include <iostream>
#include <stdexcept>
#include <string>
#include <algorithm>

using namespace std;

struct Dummy {
    static unsigned count;

    Dummy() {
        ++count;
        cout << "Dummy() @ " << this << " [" << count << "]" << endl;
    }

    ~Dummy() {
        --count;
        cout << "~Dummy() @ " << this << " [" << count << "]" << endl;
    }
};
unsigned Dummy::count = 0;


void recursiveFunc(int n) {
    Dummy dummy;
    if (n <= 1) {
        cout << "[func] base case n=1" << endl;
        return;
    }
    if (n == 5) {
        cout << "[func] **** tjohooo !!!" << endl;
        throw domain_error("jabba dabba dooo");
    }
    cout << "[func] recursive case, n=" << n << endl;
    recursiveFunc(n - 1);
}

int main() {
    cout << "# dummies = " << Dummy::count << endl;

    cout << "---- without exception ------\n";
    try {
        recursiveFunc(4);
    } catch (logic_error& err) {
        cout << "*** " << err.what() << endl;
    }
    cout << "# dummies = " << Dummy::count << endl;

    cout << "---- with exception ------\n";
    try {
        recursiveFunc(8);
    } catch (logic_error& err) {
        cout << "catch: " << err.what() << endl;
    }
    cout << "# dummies = " << Dummy::count << endl;

    return 0;
}