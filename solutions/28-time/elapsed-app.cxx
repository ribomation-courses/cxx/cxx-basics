#include <iostream>
#include <iomanip>
#include <string>
#include "elapsed.hxx"

using XXL = unsigned long long;

XXL fibRecurs(unsigned n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fibRecurs(n - 2) + fibRecurs(n - 1);
}

XXL fibIter(unsigned n) {
    XXL f0 = 0;
    XXL f1 = 1;
    while (--n > 0) {
        XXL f = f0 + f1;
        f0 = f1;
        f1 = f;
    }
    return f1;
}

int main(int argc, char** argv) {
    auto n = (argc == 1) ? 45U : stoi(argv[1]);
    {
        auto[result, ns] = elapsed<unsigned, XXL>(fibRecurs, n);
        cout << "RECURSIVE: fib(" << n << ") = " << result
             << " (elapsed " << fixed << ns * 1E-9 << " secs)\n";
    }
    {
        auto[result, ns] = elapsed<unsigned, XXL>(fibIter, n);
        cout << "ITERATIVE: fib(" << n << ") = " << result
             << " (elapsed " << fixed << ns * 1E-9 << " secs)\n";
    }

    {
        n=100;
        auto[result, ns] = elapsed<unsigned, XXL>(fibIter, n);
        cout << "ITERATIVE: fib(" << n << ") = " << result
             << " (elapsed " << fixed << ns * 1E-9 << " secs)\n";
    }

    return 0;
}
