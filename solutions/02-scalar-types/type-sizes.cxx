#include <iostream>
using namespace std;

void size(const char* type, int size) {
    cout << type << " = " << size << " bytes\n";
}

int main() {
    size("char        ", sizeof(char));
    size("short       ", sizeof(short int));
    size("int         ", sizeof(int));
    size("unsigned    ", sizeof(unsigned int));
    size("long        ", sizeof(long int));
    size("long long   ", sizeof(long long int));

    size("float       ", sizeof(float));
    size("double      ", sizeof(double));
    size("long double ", sizeof(long double));

    size("char*       ", sizeof(char*));
    size("long double*", sizeof(long double*));

    return 0;
}
