#include <iostream>
#include <string>
#include <regex>

using namespace std;
using namespace std::literals;

int main() {
    regex re{"\\d+"};

    auto        lineno = 1U;
    for (string line; getline(cin, line); ++lineno) {
        auto result = regex_replace(line, re, "#");
        cout << lineno << ") " << result << endl;
    }

    return 0;
}

