#include <utility>

#include <utility>

#include <utility>

#include <iostream>
#include <string>

using namespace std;
using namespace std::literals;

class Vehicle {
    const string license;
public:
    explicit Vehicle(string license) : license{(move(license))} {}

    virtual string toString() const {
        return "licence="s + license;
    }
};

class LandVehicle : public Vehicle {
    const unsigned short numWheels;
public:
    LandVehicle(string license, unsigned short numWheels)
            : Vehicle{move(license)}, numWheels{numWheels} {}

    string toString() const override {
        return Vehicle::toString() + ", wheels="s + to_string(numWheels);
    }
};

class AirVehicle : public Vehicle {
    const bool jet;
public:
    AirVehicle(string license, bool jet) 
    : Vehicle{move(license)}, jet{jet} {}

    string toString() const override {
        return Vehicle::toString() + ", jet="s + (jet ? "Yes" : "No");
    }
};

class JamesBondVehicle : public LandVehicle, public AirVehicle {
public:
    JamesBondVehicle(string license)
            : LandVehicle(license + "-LAND"s, 4), AirVehicle(license + "-AIR"s, true) {}

    string toString() const override {
        return LandVehicle::toString() + " + "s + AirVehicle::toString();
    }
};

void print(const Vehicle& v) {
    cout << "v: " << v.toString() << endl;
}

int main() {
    JamesBondVehicle bmw{"BMW007"};
    cout << "bmw: " << bmw.toString() << endl;
    //print(bmw);
    return 0;
}
