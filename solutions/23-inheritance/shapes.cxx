#include <iostream>
#include <string>
#include <random>
#include <vector>
#include <cassert>
#include "Shape.hxx"
#include "Rect.hxx"
#include "Circle.hxx"
#include "Triangle.hxx"
#include "Square.hxx"

using namespace std;


Shape* mkShape() {
    static random_device  r;
    uniform_int_distribution<int> nextShape{1, 4};
    uniform_int_distribution<int> nextValue{1, 10};

    cout << "****\n";
    switch (nextShape(r)) {
        case 1:
            return new Rect{nextValue(r), nextValue(r)};
        case 2:
            return new Circle{nextValue(r)};
        case 3:
            return new Triangle{nextValue(r), nextValue(r)};
        case 4:
            return new Square{nextValue(r)};
    }

    throw domain_error("WTF: this should not happen");
}

vector<Shape*> mkShapes(int n) {
    vector<Shape*> shapes;
    while (n-- > 0) shapes.push_back(mkShape());
    return shapes;
}

void print(const Shape& s) {
    cout << s << endl;
}

int main(int numArgs, char* args[]) {
    auto N = (numArgs <= 1) ? 5U : stoi(args[1]);

    auto shapes = mkShapes(N);

    cout << "---------------------\n";
    for (auto s : shapes) print(*s);

    cout << "---------------------\n";
    for (Shape* s = shapes.back(); !shapes.empty(); shapes.pop_back(), s = shapes.back()) {
        cout << "****\n";
        delete s;
    }
    assert(shapes.empty());

    return 0;
}
