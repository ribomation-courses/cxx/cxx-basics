#include <iostream>
#include <string>
#include "fibonacci.hxx"
using namespace std;
using namespace std::literals;
static int answer() {return 17;}

int main(int argc, char** argv) {
    auto arg = (argc == 1) ? 42U : stoi(argv[1]);
    cout << "fib(" << arg << ") = " << fibonacci(arg) << endl;
    cout << "answer: " << answer() << endl;
    cout << "tjollahopp: " << tjollahopp << endl;
    return 0;
}
