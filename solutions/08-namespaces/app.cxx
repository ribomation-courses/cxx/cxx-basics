#include <iostream>
#include "silly.hxx"
using namespace std;
//using namespace se::ribomation::app;
using se::ribomation::app::answer;
using se::ribomation::app::Silly;

int main() {
    auto result = answer();
    cout << "The answer to everything = "
         << result << endl;

    Silly s = {17};
    cout << "The answer to something  = "
         << s.number << endl;

    cout << "The answer to anything   = "
         << Silly{4711}.number << endl;

    return 0;
}
