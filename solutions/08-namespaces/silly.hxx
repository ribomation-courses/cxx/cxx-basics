#pragma once
namespace se {
    namespace ribomation {
        namespace app {
            struct Silly {
                int number = 42;
            };
            int answer();
        }
    }
}

