#pragma  once

#include <iostream>
#include <sstream>
#include <string>

namespace persons {
    using namespace std;
    using namespace std::literals;

    class Person {
        string     name;
        unsigned   age;
        static int instanceCount;

    public:
        Person(string name, unsigned age = 42) : name{name}, age{age} {
            ++instanceCount;
            cout << "Person(" << name << "," << age << ") @ " << this << " [" << instanceCount << "]" << endl;
        }

        Person(const Person& that) : name{that.name}, age{that.age} {
            ++instanceCount;
            cout << "Person(const Person&: " << &that << ") @ " << this << " [" << instanceCount << "]" << endl;
        }

        ~Person() {
            --instanceCount;
            cout << "~Person() @ " << this << " [" << instanceCount << "]" << endl;
        }

        operator string() const { return name; }
        operator unsigned() const { return age; }

        string getName() const { return name; }

        void setName(const string& name) { Person::name = name; }

        unsigned getAge() const { return age; }

        void setAge(unsigned age) { Person::age = age; }

        static int getCount() { return instanceCount; }

        string toString() {
            ostringstream buf;
            buf << "Person{"
                << getName() << ", " << getAge()
                << "}"
                << " [" << getCount() << "]";
            return buf.str();
        }
    };

}
