#include <iostream>
#include <string>
#include "person.hxx"
#include "person-factory.hxx"

using namespace std;
using namespace std::literals;
using namespace persons;

Person print(Person q) {
    cout << "[print] q = " << q.toString() << endl;
    q.setAge(q.getAge() + 10);
    return q;
}

int main() {
    auto f = PersonFactory{};
    cout << "# persons = " << Person::getCount() << endl;
    {
        auto p = f.create();
        cout << "[main] p = " << p.toString() << endl;
        auto r = print(p);
        cout << "[main] r = " << r.toString() << endl;
    }
    cout << "# persons = " << Person::getCount() << endl;

    {
        auto     a   = f.create();
        unsigned age = a;
        cout << "age = " << age << endl;

        string name = a;
        cout << "name = " << name << endl;

        Person p = "Bosse"s;
        cout << "p = " << p.toString() << endl;

        print("Olle"s);
    }
    cout << "# persons = " << Person::getCount() << endl;

    return 0;
}
