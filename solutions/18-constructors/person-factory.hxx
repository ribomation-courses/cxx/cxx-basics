#pragma once

#include <string>
#include <vector>
#include <random>
#include "person.hxx"


namespace persons {
    using namespace std;
    using namespace std::literals;

    class PersonFactory {
        random_device  r;
        vector<string> names = {"Anna"s, "Berit"s, "Carin"s, "Doris"s, "Eva"s, "Frida"s};

    public:
        Person create() {
            unsigned const                     N = names.size() - 1;
            uniform_int_distribution<unsigned> nextNameIdx{0, N};
            uniform_int_distribution<unsigned> nextAge{20, 80};

            auto name = names[nextNameIdx(r)];
            auto age  = nextAge(r);
            return {name, age};
        }
    };

}
