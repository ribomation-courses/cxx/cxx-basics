#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <utility>
#include <random>
#include <cctype>

using namespace std;
using namespace std::string_literals;

using Frequencies = unordered_map<string, unsigned>;
using WordFreq    = pair<string, unsigned>;

auto load(istream&, unsigned) -> Frequencies;
auto mostFrequent(Frequencies, unsigned) -> vector<WordFreq>;
auto extractWords(string, unsigned) -> vector<string>;
auto toTags(vector<WordFreq>) -> vector<string>;
auto join(vector<string>, string) -> string;
auto randColor() -> string;


int main(int argc, char** argv) {
    auto maxWords = 100U;
    auto minSize  = 5U;

    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-w")
            maxWords = static_cast<unsigned>(stoi(argv[++k]));
        else if (arg == "-s")
            minSize = static_cast<unsigned>(stoi(argv[++k]));
    }

    auto freqs = load(cin, minSize);
    auto words = mostFrequent(freqs, maxWords);
    auto tags  = toTags(words);

    random_device r;
    shuffle(tags.begin(), tags.end(), r);

    const string htmlPrefix = R"(
        <html>
        <head>
            <title>Word Cloud</title>
            <style>
                body {font-family: sans-serif; font-size: 16pt;}
                h1   {text-align: center; color: orange; font-size: 200%;}
            </style>
        </head>
        <body>
        <h1>Word Cloud</h1>
    )";
    const string htmlSuffix = R"(
        </body>
        </html>
    )";

    const auto html = join(tags, "\n");
    cout << htmlPrefix << html << htmlSuffix << endl;
    cerr << "Written " << html.size() << " chars\n";

    return 0;
}

Frequencies load(istream& in, unsigned minSize) {
    Frequencies freqs;
    for (string line; getline(in, line);) {
        for (auto& w : extractWords(line, minSize)) { freqs[w]++; }
    }
    return freqs;
}

vector<WordFreq> mostFrequent(Frequencies freqs, unsigned int N) {
    vector<WordFreq> sortable{freqs.begin(), freqs.end()};

    sort(sortable.begin(), sortable.end(), [](auto left, auto right) {
        return left.second > right.second;
    });

    vector<WordFreq> mostFrequent;
    copy_n(sortable.begin(), N, back_inserter(mostFrequent));
    return mostFrequent;
}

vector<string> toTags(vector<WordFreq> words) {
    auto   maxFontSize = 150.0;
    auto   minFontSize = 10.0;
    auto   maxFreq     = words.front().second;
    auto   minFreq     = words.back().second;
    double scale       = (maxFontSize - minFontSize) / (maxFreq - minFreq);

    vector<string> tags;
    for (const auto& [word, freq] : words) {
        auto          fontSize = static_cast<unsigned>(freq * scale) + minFontSize;
        ostringstream buf;
        buf << "<span style='font-size:" << fontSize << "px;" << "color:#" << randColor() << ";'>" << word << "</span>";
        tags.push_back(buf.str());
    }
    return tags;
}

string join(vector<string> strings, string delim) {
    return accumulate(strings.begin(), strings.end(), ""s, [=](auto acc, auto str) {
        return acc + delim + str;
    });
}

vector<string> extractWords(string line, unsigned minSize) {
    transform(line.begin(), line.end(), line.begin(), [](auto ch) {
        return isalpha(ch) ? tolower(ch) : ' ';
    });

    vector<string> result;
    istringstream  buf{line};
    for (string    word; buf >> word;) {
        if (word.size() >= minSize) {
            result.push_back(word);
        }
    }

    return result;
}

string randColor() {
    static random_device               r;
    uniform_int_distribution<unsigned> color{0, 255};
    ostringstream                      buf;

    buf << hex << color(r) << color(r) << color(r);
    return buf.str();
}
