#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <random>
#include <cctype>

using namespace std;
using namespace std::literals;
using WordFreq = pair<string, unsigned>;

const string prefix = "<html><head></head><body>\n";
const string suffix = "\n</body></html>";

string randColor() {
    static random_device               r;
    uniform_int_distribution<unsigned> color{0, 255};
    ostringstream                      buf;
    buf << "#" << hex << color(r) << color(r) << color(r);
    return buf.str();
}

int main(int argc, char** argv) {
    unsigned long minWordSize = 4;
    unsigned      maxWords    = 100;

    auto        freqs = unordered_map<string, unsigned>{};
    for (string line; getline(cin, line);) {
        transform(line.begin(), line.end(), line.begin(), [](auto ch) {
            return ::isalpha(ch) ? ::tolower(ch) : ' ';
        });
        istringstream buf{line};
        for (string   word; buf >> word;) {
            if (word.size() >= minWordSize) {
                ++freqs[word];
            }
        }
    }

    auto sortable = vector<WordFreq>{freqs.begin(), freqs.end()};
    sort(sortable.begin(), sortable.end(), [](WordFreq lhs, WordFreq rhs) {
        return rhs.second < lhs.second;
    });

    auto words = vector<WordFreq>{};
    copy_n(sortable.begin(), maxWords, back_inserter(words));

    auto   maxFreq     = words.front().second;
    auto   minFreq     = words.back().second;
    auto   maxFontSize = 150.0;
    auto   minFontSize = 10.0;
    double scale       = (maxFontSize - minFontSize) / (maxFreq - minFreq);

    auto tags = vector<string>{};
    transform(words.begin(), words.end(), back_inserter(tags), [=](WordFreq wf) {
        auto          word     = wf.first;
        auto          freq     = wf.second;
        auto          fontSize = static_cast<unsigned>(freq * scale) + minFontSize;
        ostringstream buf;
        buf << "<span style=\"font-size:" << fontSize << "pt; color:"
            << randColor() << ";\" >" << word << "</span>";
        return buf.str();
    });

    random_device r;
    shuffle(tags.begin(), tags.end(), r);

    auto html = accumulate(tags.begin(), tags.end(), ""s, [](auto result, auto tag) {
        return result + "\n"s + tag;
    });

    cout << prefix << html << suffix << endl;

    return 0;
}
