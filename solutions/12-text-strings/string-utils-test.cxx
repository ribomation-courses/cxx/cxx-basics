#include <iostream>
#include <cassert>
#include "string-utils.hxx"

using namespace std;
using namespace std::literals;
using namespace StringUtils;


int main() {
    assert(toLowerCase("Tjolla Hopp 123 ABC"s) == "tjolla hopp 123 abc"s);
    assert(toUpperCase("Tjolla Hopp 123 ABC"s) == "TJOLLA HOPP 123 ABC"s);
    assert(capitalize("hello"s) == "Hello"s);
    assert(capitalize("HELLO"s) == "Hello"s);
    assert(strip("Tjolla Hopp 123 ABC"s) == "TjollaHoppABC"s);
    assert(truncate("Tjolla Hopp 123 ABC"s, 6) == "Tjolla"s);
    assert(truncate("ABC"s, 6) == "ABC"s);

    assert(startsWith("abcdef"s, "abc"s) == true);
    assert(startsWith("abcdef"s, "ABC"s) == false);
    assert(startsWith("abc"s, "abcX"s) == true);

    assert(endsWith("abcdef"s, "ef"s) == true);
    assert(endsWith("abcdef"s, "EF"s) == false);
    assert(endsWith("abc"s, "Xabc"s) == true);

    cout << "All tests passed\n";
    return 0;
}
