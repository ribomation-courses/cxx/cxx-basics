#pragma once

#include <string>
#include <string_view>
#include <cctype>

namespace StringUtils {
    using namespace std;

    inline string toLowerCase(string s) {
        for (auto k = 0U; k < s.size(); ++k) {
            s[k] = static_cast<char>(tolower(s[k]));
        }
        return s;
    }

    inline string toUpperCase(string s) {
        for (auto k = 0U; k < s.size(); ++k) {
            s[k] = static_cast<char>(toupper(s[k]));
        }
        return s;
    }

    inline string capitalize(string s) {
        return toUpperCase(s.substr(0, 1)) + toLowerCase(s.substr(1));
    }

    inline string strip(string s) {
        string    result;
        for (auto ch : s) if (isalpha(ch)) result += ch;
        return result;
    }

    inline string truncate(string s, const unsigned width) {
        if (s.size() <= width) return s;
        return s.substr(0, width);
    }

    inline bool startsWith(const string& payload, const string& prefix) {
        if (prefix.size() > payload.size()) {
            return startsWith(payload, prefix.substr(0, payload.size()));
        }

        string_view sub{payload.data(), prefix.size()};
        return sub == prefix;
    }

    inline bool endsWith(const string& payload, const string& suffix) {
        if (suffix.size() > payload.size()) {
            return endsWith(payload, suffix.substr(suffix.size() - payload.size(), payload.size()));
        }

        string_view sub{payload.data() + payload.size() - suffix.size(), suffix.size()};
        return sub == suffix;
    }

}
