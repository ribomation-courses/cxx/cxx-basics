#pragma once
#include <string>

namespace se::ribomation::bank {
    using namespace std;
    enum class AccountType {
        savings, check, transfer
    };
    using BalanceType = int;
    using RateType = double;

    struct Account {
        AccountType type    = AccountType::transfer;
        BalanceType balance = 100;
        RateType    rate    = 1.5;
    };
    string asString(AccountType);
    string asString(const Account&);
}
