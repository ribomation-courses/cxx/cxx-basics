#include "account.hxx"

namespace se::ribomation::bank {
    using namespace std;
    using namespace std::literals;

    string asString(AccountType type) {
        switch (type) {
            case AccountType::check   :
                return "check   "s;
            case AccountType::savings :
                return "savings "s;
            case AccountType::transfer:
                return "transfer"s;
        }
        return "???";
    }

    string asString(const Account& acc) {
        return "Account{"s + asString(acc.type)
               + ", SEK "s + to_string(acc.balance)
               + ", "s + to_string(acc.rate) + "%}"s;
    }
}
