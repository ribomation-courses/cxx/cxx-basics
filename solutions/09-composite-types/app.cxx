#include <iostream>
#include "account.hxx"
using namespace std;
using namespace se::ribomation::bank;

int main() {
    Account a1{AccountType::transfer, -125, 0.05};
    Account a2{AccountType::savings, 7500, 3.75};
    Account a3{AccountType::check, 1024, 1.25};

    cout << "a1: " << asString(a1) << endl;
    cout << "a2: " << asString(a2) << endl;
    cout << "a3: Account{" << asString(a3.type)
                               << ", SEK " << a3.balance
                               << ", " << a3.rate
                               << "%}" << endl;

    return 0;
}
