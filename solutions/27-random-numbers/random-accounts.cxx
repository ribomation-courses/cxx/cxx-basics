#include <iostream>
#include <random>
#include <algorithm>
#include <vector>
#include "account.hxx"

using namespace std;
using namespace std::literals;

random_device r;

struct AccountGenerator {
    AccountGenerator() = default;

    Account operator()() { return nextAccount(); }

    Account nextAccount() {
        normal_distribution<double>      nextBalance{500, 400};
        uniform_real_distribution<float> nextRate{0.5, 4.5};
        return {nextAccno("SEB###-#####"), static_cast<int>(nextBalance(r)), nextRate(r)};
    }

    string nextAccno(string pattern) {
        uniform_int_distribution<unsigned char> nextDigit{'0', '9'};
        transform(pattern.begin(), pattern.end(), pattern.begin(), [&](auto ch) {
            return ch == '#' ? nextDigit(r) : ch;
        });
        return pattern;
    }
};

int main(int argc, char** argv) {
    auto N = argc == 1 ? 100U : stoi(argv[1]);

    vector<Account>  accounts;
    AccountGenerator g;
    generate_n(back_inserter(accounts), N, g);
    for_each(accounts.begin(), accounts.end(), [](auto& acc) {
        cout << acc << endl;
    });

    return 0;
}
