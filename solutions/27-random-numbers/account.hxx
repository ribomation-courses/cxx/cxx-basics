#pragma once

#include <string>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace std::literals;

class Account {
    const string accno;
    int          balance;
    float        rate;
public:
    Account(const string& accno, int balance, float rate)
            : accno(accno), balance(balance), rate(rate) {}

    string toString() const {
        ostringstream buf;
        buf << "Account{" << accno
            << ", SEK " << setw(6) << right << balance
            << ", " << setprecision(2) << rate << "%}";
        return buf.str();
    }

    friend ostream& operator<<(ostream& os, const Account& acc) {
        return os << acc.toString();
    }
};
