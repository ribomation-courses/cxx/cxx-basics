#include <iostream>
#include <string>
using namespace std;

int main(int argc, char** argv) {
    string name = (argc <= 1) ? "Jens" : argv[1];

    cout << "Tjenare " << name
         << ". Välkommen till C++" << endl;

    operator<<( operator<<( operator<<(cout, "Hello "), name ), "\n");

    return 0;
}
