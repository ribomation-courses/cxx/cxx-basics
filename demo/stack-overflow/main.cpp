#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <csignal>
#include <unistd.h>

using namespace std;
using namespace std::literals;

unsigned long long numStackFrames = 0;

void recurse() {
    ++numStackFrames;
    int filler[] = {1, 2, 3, 4};
    if (numStackFrames % (10 * 1024) == 0) {
        printf("num stack-frames=%lld\n", numStackFrames);
    }
    usleep(1);
    recurse();
}

void signalHandler(int signo) {
    printf("signal: %s (%d), num stack-frames=%lld\n",
           strsignal(signo), signo, numStackFrames);
    exit(2);
}

char altSignalStack[SIGSTKSZ];

int main() {
    printf("sizeof(altSignalStack) = %ld\n", sizeof(altSignalStack));

    stack_t altStackConfig;
    altStackConfig.ss_sp    = altSignalStack;
    altStackConfig.ss_size  = sizeof(altSignalStack);
    altStackConfig.ss_flags = 0;
    if (int rc = sigaltstack(&altStackConfig, nullptr); rc == -1) {
        printf("sigaltstack: %s\n", strerror(errno));
        exit(1);
    }

    struct sigaction action{};
    action.sa_handler = &signalHandler;
    action.sa_flags   = SA_ONSTACK;
    sigemptyset(&action.sa_mask);
    if (int rc = sigaction(SIGSEGV, &action, nullptr); rc == -1) {
        printf("sigaction: %s\n", strerror(errno));
        exit(1);
    }

    recurse();

    return 0;
}
